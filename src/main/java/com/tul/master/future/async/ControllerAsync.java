package com.tul.master.future.async;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.DefaultAsyncHttpClient;
import org.asynchttpclient.Response;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControllerAsync {

    private AsyncHttpClient httpClient = new DefaultAsyncHttpClient();

    @GetMapping("/fibb/{n}")
    CompletableFuture<Long> getFibb(@PathVariable("n") long n) {
        if (n < 2) {
            return CompletableFuture.completedFuture(Long.valueOf("1"));

        } else {
            CompletionStage<Long> fibb1 = requestFibb(n - 1);
            CompletionStage<Long> fibb2 = requestFibb(n - 2);
            return fibb1
                    .thenCombine(fibb2, Long::sum).toCompletableFuture();
        }
    }

    private CompletionStage<Long> requestFibb(long n) {
        CompletableFuture<Response> responseCompletableFuture = httpClient.prepareGet("http://localhost:8080/fibb/" + n)
                .execute().toCompletableFuture();
        return responseCompletableFuture.thenApply(resp -> Long.parseLong(resp.getResponseBody()));
    }
}
