package com.tul.master.future.sync;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.DefaultAsyncHttpClient;
import org.asynchttpclient.Response;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControllerSync {

    private AsyncHttpClient httpClient = new DefaultAsyncHttpClient();

    @GetMapping("/fibb/{n}")
    long getFibb(@PathVariable("n") long n) throws ExecutionException, InterruptedException {
        if (n < 2) {
            return 1;

        } else {
            CompletionStage<Long> fibb1 = requestFibb(n - 1);
            CompletionStage<Long> fibb2 = requestFibb(n - 2);
            return fibb1
                    .thenCombine(fibb2, Long::sum)
                    .toCompletableFuture()
                    .get();
        }
    }

    private CompletionStage<Long> requestFibb(long n) {
        CompletableFuture<Response> responseCompletableFuture = httpClient.prepareGet("http://localhost:8080/fibb/" + n)
                .execute().toCompletableFuture();
        return responseCompletableFuture.thenApply(resp -> Long.parseLong(resp.getResponseBody()));
    }
}
