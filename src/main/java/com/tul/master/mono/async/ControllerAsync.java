package com.tul.master.mono.async;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@RestController
public class ControllerAsync {

    private WebClient httpClient = WebClient.create();

    @GetMapping("/fibb/{n}")
    Mono<Long> getFibb(@PathVariable("n") long n) {
        if (n < 2) {
            return Mono.just(Long.parseLong("1"));
        } else {
            Mono<Long> fibb1 = requestFibb(n - 1);
            Mono<Long> fibb2 = requestFibb(n - 2);
            return fibb1
                    .zipWith(fibb2)
                    .map(res -> res.getT1() + res.getT2());
        }
    }

    private Mono<Long> requestFibb(long n) {
        return httpClient.get()
                .uri("http://127.0.0.1:8081/fibb/" + n)
                .retrieve()
                .bodyToMono(Long.class);
    }
}
