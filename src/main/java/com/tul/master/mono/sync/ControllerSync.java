package com.tul.master.mono.sync;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

@RestController
public class ControllerSync {

    private WebClient httpClient = WebClient.create();

    @GetMapping("/fibb/{n}")
    long getFibb(@PathVariable("n") long n) {
        if (n < 2) {
            return 1;
        } else {
            long fibb1 = requestFibb(n - 1);
            long fibb2 = requestFibb(n - 2);
            return fibb1 + fibb2;
        }
    }

    private Long requestFibb(long n) {
        return httpClient.get()
                .uri("http://127.0.0.1:8080/fibb/" + n)
                .retrieve()
                .bodyToMono(Long.class)
                .block();
    }
}
