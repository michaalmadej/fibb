package com.tul.master.ratpack;

import java.net.InetAddress;
import java.net.URI;
import java.time.Duration;

import ratpack.exec.Promise;
import ratpack.http.client.HttpClient;
import ratpack.http.client.ReceivedResponse;
import ratpack.server.RatpackServer;

public class App {

    public static void main(String[] args) throws Exception {
        final HttpClient httpClient = HttpClient.of(
                httpClientConfig -> httpClientConfig.readTimeout(Duration.ofMinutes(2))
        );

        RatpackServer.start(server -> server.serverConfig(
                config -> config
                        .port(8081)
                        .address(InetAddress.getByName("0.0.0.0"))
                        .threads(1))
                .handlers(chain -> chain
                        .prefix("fibb", fib -> fib.get(":n", ctx -> {
                            final long n = Long.parseLong(ctx.getPathTokens().get("n"));
                            if (n < 2) {
                                ctx.render("1");
                            } else {
                                final Promise<ReceivedResponse> receivedFibb1 =
                                        httpClient.get(new URI("http://127.0.0.1:8081/fibb/" + (n - 1)));
                                final Promise<Long> promiseFibb1 = receivedFibb1
                                        .map(br -> Long.parseLong(br.getBody().getText()));
                                final Promise<ReceivedResponse> receivedFibb2 =
                                        httpClient.get(new URI("http://127.0.0.1:8081/fibb/" + (n - 2)));
                                final Promise<Long> promiseFibb2 = receivedFibb2
                                        .map(br -> Long.parseLong(br.getBody().getText()));
                                promiseFibb1.then(fib1 -> promiseFibb2
                                        .then(fib2 -> ctx.render(String.valueOf(fib1 + fib2))));
                            }

                        })))
        );
    }
}
