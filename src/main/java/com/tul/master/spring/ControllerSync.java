package com.tul.master.spring;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse.BodyHandlers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControllerSync {

    private HttpClient client = HttpClient.newHttpClient();

    @GetMapping("/fibb/{n}")
    long getFibb(@PathVariable("n") long n) throws IOException, InterruptedException {
        if (n < 2) {
            return 1;
        } else {
            String fibb1 = client.send(createRequest(n - 1), BodyHandlers.ofString()).body();
            String fibb2 = client.send(createRequest(n - 2), BodyHandlers.ofString()).body();
            return Long.parseLong(fibb1) + Long.parseLong(fibb2);
        }
    }

    private HttpRequest createRequest(long n) {
        return HttpRequest.newBuilder()
                .uri(URI.create("http://127.0.0.1:8080/fibb/" + n))
                .build();
    }
}
